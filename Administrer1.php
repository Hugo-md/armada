<?php session_start () ?>
<?php 
    if($_SESSION['Role'] != 'Admin')
    {
        echo "<script>alert('Error SYSTEM, Reset !');location.href='index.php';</script>";
    }
?>
<?php
include 'fonction.inc.php'
?>

<?php $id=$_GET['tsp']; ?>

<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Administrer</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
    <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <a class="btn btn-dark" href="deco.php">Déconnection</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                    <a href="PageAcceuil1.php"><img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/></a>
                    </div>
                </div>
            
                <br />

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        $Personnes = AfficheUnePersonne($id);
                        if ($Personnes == null) {
                            echo "<h2>Personne n'est inscrit sur le site pour le moment.</h2>";
                            exit();
                        }
                        else {
                        }
                        while ($personne = mysqli_fetch_array($Personnes)) {
                            echo '<div class="row" style="background-color: black;" >';
                                echo '<div class="col-lg-4 col-sm-4" style="text-align : center;">';
                                    echo '<h4 style="color:white;">Nom : '.$personne['Nom'].'</h4>';
                                    echo '<h4 style="color:white;">Prenom : '.$personne['Prenom'].'</h4>';
                                echo '</div>';
                                echo '<div class="col-lg-4 col-sm-8" style="text-align : center;">';
                                    echo '<h4 style="color:white;">Date de naissance : '.$personne['Naissance'].'</h4>';
                                    echo '<h4 style="color:white;">Email : '.$personne['Mail'].'</h4>';
                                    echo '<h4 style="color:white;">Role : '.$personne['Role'].'</h4><br /><br />';
                                echo '</div>';
                                echo '<div class="col-lg-4" style="text-align : center;">';
                                echo '</div>';
                            echo '</div>';
                            echo '<br /><br />';   
                        }
                        ?>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-4" style="text-align : center;">
                        <form method="post" action="administre.php" enctype="multipart/form-data">
                            <input type="hidden" name="IDpers" value="<?php echo $id ?>" />
                            <input type="hidden" name="change" value="Inscrit" />
                            <input type="submit" class="btn btn-dark" style="font-size:25pt" value="Inscrit"></button><br />
                        </form>
                    </div>
                    <div class="col-lg-4 col-sm-4" style="text-align : center;">
                        <form method="post" action="administre.php" enctype="multipart/form-data">
                            <input type="hidden" name="IDpers" value="<?php echo $id ?>" />  
                            <input type="hidden" name="change" value="Capitaine" />
                            <input type="submit" class="btn btn-dark" style="font-size:25pt" value="Capitaine"></button><br />
                        </form>
                    </div>
                    <div class="col-lg-4 col-sm-4" style="text-align : center;">
                        <form method="post" action="administre.php" enctype="multipart/form-data">
                            <input type="hidden" name="IDpers" value="<?php echo $id ?>" />
                            <input type="hidden" name="change" value="Admin" />
                            <input type="submit" class="btn btn-dark" style="font-size:25pt" value="Admin"></button><br />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
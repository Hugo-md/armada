<?php session_start () ?>
<?php include 'fonction.inc.php' ?>
<?php
    $nom=$_POST['nom'];
    $descr=$_POST['desc'];
    $ima=$_FILES['imag']['name'];
    $tamponnom=$_POST['nombat'];
    $tamponid=$_POST['IDbat'];
    $extensions_ok = array('jpg', 'png', 'JPG', 'PNG');
    $extensions_fichier = substr(strrchr($_FILES['imag']['name'], '.'), 1);
    $destination = 'image/'.$ima;

    if(empty($nom) OR empty($descr)) 
    { 
        echo '<font color="red">Attention, un ou plusieurs champs ne sont pas remplis</font>'; 
    } 
    else
    {
        $test = getNomBateauPersonne($nom, $tamponnom, $tamponid);
        if($test == 1)
        {
            echo '<font color="red">Le nom du bateau existe déjà</font>';
        }
        else
        {
            
            ModifierBateau($nom, $descr, $tamponnom, $tamponid);

            $bateaux = AfficheUnDetail($tamponid, $tamponnom);
            if ($bateaux == null) {
            }
            else {
                UpdateNomDetail($nom, $tamponnom, $tamponid);
            }

            echo '<font color="green">Bateau modifié.</font>';
            header('Location: PageAcceuil1.php');
            exit();
        }
    }
?>
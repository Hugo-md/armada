<?php session_start () ?>
<?php 
    if($_SESSION['Role'] != 'Capitaine')
    {
        echo "<script>alert('Error SYSTEM, Reset !');location.href='index.php';</script>";
    }
?>
<?php
include 'fonction.inc.php'
?>
<?php 
   $bateaux = AfficheUnBateau($_SESSION['ID'], $_GET['nom']);
   while ($bateau = mysqli_fetch_array($bateaux))
   {
       $nomb=$bateau['Nom'];
       $nomi=$bateau['Image'];
       $idbat=$bateau['IDbateau'];
   }
?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Supprimer un bateau</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
    <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <a class="btn btn-dark" href="deco.php">Déconnection</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                    <a href="PageAcceuil1.php"><img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/></a>
                    </div>
                </div>
            
                <br />

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        $bateaux = AfficheUnBateau($_SESSION['ID'], $_GET['nom']);
                        if ($bateaux == null) {
                            echo "<h2>Vous n'avez ajouté aucun bateau pour le moment.</h2>";
                            exit();
                        }
                        else {
                        }
                        echo '<h1 style="font-size: 35px; text-align : center;">Etes vous sur de vouloir supprimer ce bateau</h1>';
                        while ($bateau = mysqli_fetch_array($bateaux)) {
                            echo '<div class="row" style="background-color: black;" >';
                                echo '<div class="col-lg-3 col-sm-3" >';
                                    echo '<img class="img-fluid" style="max-height: 350px;" src="image/'.$bateau['Image'].'"/>';
                                echo '</div>';
                                echo '<div class="col-lg-9 col-sm-9" >';
                                    echo '<h4 style="color:white;">Nom : '.$bateau['Nom'].'</h4><br />';
                                    echo '<p style="color:white;">'.$bateau['Description'].'</p><br /><br />';
                                echo '</div>';
                            echo '</div>';
                            echo '<br /><br />';   
                        }
                        ?>
                        <form method="post" action="delbateau.php" enctype="multipart/form-data">
                            <br />
                            <input type="hidden" name="IDbat" value="<?php echo $idbat ?>" />
                            <input type="hidden" name="nombat" value="<?php echo $nomb ?>" />
                            <input type="hidden" name="nomfich" value="<?php echo $nomi ?>" />
                            <p style="text-align : center;"><input type="submit" class="btn btn-dark" value="Oui"></button><br /></p>
                        </form>
                        <p style="text-align : center;"><a class="btn btn-dark" href="PageAcceuil1.php">Non</a></p>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
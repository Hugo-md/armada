<?php session_start () ?>
<?php include 'fonction.inc.php' ?>
<?php
    $nom=$_POST['nom'];
    $descr=$_POST['desc'];
    $ima=$_FILES['imag']['name'];
    $extensions_ok = array('jpg', 'png', 'JPG', 'PNG');
    $extensions_fichier = substr(strrchr($_FILES['imag']['name'], '.'), 1);
    $destination = 'image/'.$ima;

    if(empty($nom) OR empty($descr) OR empty($ima)) 
    { 
        echo '<font color="red">Attention, un ou plusieurs champs ne sont pas remplis</font>'; 
    } 
    else
    {
        $test = getNomBateau($nom);
        if($test == 1)
        {
            echo '<font color="red">Le nom du bateau existe déjà</font>';
        }
        else
        {
            if($_FILES['imag']['size'] == 0)
            {
                echo '<font color="red">Le fichier est trop gros</font>';
            }
            else
            {
                if( !in_array($extensions_fichier, $extensions_ok))
                {
                    echo '<font color="red">Le fichier doit terminer par .png ou .jpg</font>';
                }
                else
                {
                    $testnom = getNomImage($ima);
                    if($testnom == 1)
                    {
                        echo '<font color="red">Le nom de se ficher existe déjà, veuillez changer le nom et réessayer. </font>';
                    }
                    else
                    {
                        $resultat = move_uploaded_file($_FILES['imag']['tmp_name'], $destination);
                        if($resultat)
                        {
                            $con = Connection();
                            $sql = mysqli_prepare($con, 'INSERT INTO bateau(`Nom`, `Description`, `Image`, `IDbateau`) VALUES ( ?, ?, ?, "10")');
                            mysqli_stmt_bind_param($sql, 'sss', $nom, $descr, $ima);
                            mysqli_stmt_execute($sql);
                            mysqli_close($con);
                            echo '<font color="green">Bateau ajouté.</font>';
                        }
                        else
                        {
                            echo '<font color="red">Problème lors de l\'upload, veuillez réessayer</font>'; 
                        }
                    }
                }
            }
        }
    }
?>
<?php session_start () ?>
<?php
include 'fonction.inc.php'
?>
<?php 
    if($_SESSION['Role'] != 'Capitaine')
    {
        echo "<script>alert('You died !');location.href='index.php';</script>";
    }
?>
<?php 
   $bateaux = getBateau($_SESSION['ID'], $_GET['nom']);
   while ($bateau = mysqli_fetch_array($bateaux))
   {
       $nomb=$bateau['Nom'];
       $descb=$bateau['Description'];
       $nomi=$bateau['Image'];
       $idbat=$bateau['IDbateau'];
   }
?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Modifier un bateau</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
    <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <a class="btn btn-dark" href="deco.php">Déconnection</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                    <a href="PageAcceuil1.php"><img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/></a>
                    </div>
                </div>
            
                <br />

                <div class="row">
                    <div class="col-lg-3 " ></div>

                    <div class="col-lg-6 text-center" style="text-align : center; background-color: rgba(238, 235, 235, 0.5); border-radius:7px;">
                        <form method="post" action="modbateau.php" enctype="multipart/form-data">
                            <br />
                            <h3>Nom du bateau :</h3>
                            <input type="text" name="nom" size="30" maxlength="30" value="<?php echo $nomb; ?>"/>

                            <br /><br />
                            <h3>Description non détaillée du bateau :</h3>
                            <TEXTAREA name="desc" rows="13" cols="60" maxlength="500"><?php echo $descb; ?></TEXTAREA>

                            <input type="hidden" name="IDbat" value="<?php echo $idbat ?>" />
                            <input type="hidden" name="nombat" value="<?php echo $nomb ?>" />

                            <br /><br />
                            <input type="submit" class="btn btn-dark" value="Enregistrer"></button><br />
                        </form>
                    </div>
                    <div class="col-lg-3"></div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
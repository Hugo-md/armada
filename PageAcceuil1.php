<?php session_start () ?>
<?php 
    if($_SESSION['Role'] != 'Capitaine' AND $_SESSION['Role'] != 'Admin' AND $_SESSION['Role'] != 'Inscrit')
    {
        echo "<script>alert('Retour à zéro !');location.href='index.php';</script>";
    }
?>
<?php
include 'fonction.inc.php'
?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Page d'accueil</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
        <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <?php if($_SESSION['Role']=='Capitaine'){?>
                    <a class="btn btn-dark" href="PageBateau.php">Ajouter un bateau</a>
                    <a class="btn btn-dark" href="detail.php">Ajouter info détaillée</a>   
                <?php } ?>
                <?php if($_SESSION['Role']=='Admin'){?>
                    <a class="btn btn-dark" href="Administrer.php">Administration</a>   
                <?php } ?>
                <a class="btn btn-dark" href="deco.php">Déconnection</a>
            </div>
        </div>

        <div class="row" style="background-color: rgba(50, 50, 50, 0.5);">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                        <img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/>
                    </div>
                </div>
            
                <br /><br /><br /><br /><br /><br />

                <div class="row" style="background-color: rgba(50, 50, 50, 0.5);">
                    <div class="col-lg-12">
                        <?php
                        $bateaux = AfficheBateau();
                        if ($bateaux == null) {
                            echo "<h2>Aucun bateau</h2>";
                            exit();
                        }
                        else {
                        }
                        
                        while ($bateau = mysqli_fetch_array($bateaux)) {
                            echo '<div class="row" style="background-color: black;" >';
                                echo '<div class="col-lg-3 col-sm-3" >';
                                    echo '<a style="color:white;" href="PageDetail.php?nom='.$bateau['Nom'].'&i='.$bateau['IDbateau'].'"><img class="img-fluid" style="max-height: 350px;" src="image/'.$bateau['Image'].'"/>';
                                echo '</div>';
                                echo '<div class="col-lg-9 col-sm-9" >';
                                    echo '<h4>Nom : '.$bateau['Nom'].'</h4><br />';
                                    echo '<p>'.$bateau['Description'].'</p></a><br /><br />';
                                echo '</div>';
                            echo '</div>';
                            echo '<br /><br />';    
                         }
                        ?>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
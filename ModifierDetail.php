<?php session_start () ?>
<?php
include 'fonction.inc.php'
?>
<?php 
    if($_SESSION['Role'] != 'Capitaine')
    {
        echo "<script>alert('Retour à zéro !');location.href='index.php';</script>";
    }
?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Modifier des détails</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
    <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <a class="btn btn-dark" href="deco.php">Déconnection</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                    <a href="PageAcceuil1.php"><img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/></a>
                    </div>
                </div>
            
                <br />

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        $bateaux = AfficheBateauPersonneDetail2($_SESSION['ID']);
                        if ($bateaux == null) {
                            echo "<h2>Vous n'avez ajouté aucun détail pour vos bateau pour le moment.</h2>";
                            exit();
                        }
                        else {
                        }
                        echo '<h1 style="font-size: 35px; text-align : center;">Cliquez sur le bateau à modifier les détails</h1>';
                        while ($bateau = mysqli_fetch_array($bateaux)) {
                            echo '<div class="row" style="background-color: black;" >';
                                echo '<div class="col-lg-3 col-sm-3" >';
                                    echo '<a style="color:white;" href="ModifierDetail1.php?nom='.$bateau['Nom'].'"><img class="img-fluid" style="max-height: 350px;" src="image/'.$bateau['Image'].'"/>';
                                echo '</div>';
                                echo '<div class="col-lg-9 col-sm-9" >';
                                    echo '<h4>Nom : '.$bateau['Nom'].'</h4><br />';
                                    echo '<p>'.$bateau['Description'].'</p></a><br /><br />';
                                echo '</div>';
                            echo '</div>';
                            echo '<br /><br />';   
                        }
                        ?>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
<?php session_start () ?>
<?php 
    if($_SESSION['Role'] != 'Capitaine')
    {
        echo "<script>alert('VOUS NE PASSEREZ PAS !');location.href='index.php';</script>";
    }
?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Menu détail</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
        <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <a class="btn btn-dark" href="deco.php">Déconnection</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                    <a href="PageAcceuil1.php"><img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/></a>
                    </div>
                </div>
            
                <br />

                <div class="row">
                    <div class="col-lg-12 text-center">
                    <br /><br /><br /><br /><br /><br />
                        <a class="btn btn-dark" style="font-size:40pt" href="CreerDetail.php">Ajouter des détails</a><br /><br /><br /><br />
                        <a class="btn btn-dark" style="font-size:40pt" href="ModifierDetail.php">Modifier des détails</a><br /><br /><br /><br />
                        <a class="btn btn-dark" style="font-size:40pt" href="DeleteDetail.php">Supprimer des détails</a><br /><br /><br /><br />
                    </div>       
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
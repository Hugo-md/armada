<?php session_start () ?>
<?php 
    if($_SESSION['Role'] != 'Capitaine' AND $_SESSION['Role'] != 'Admin' AND $_SESSION['Role'] != 'Inscrit')
    {
        echo "<script>alert('Retour à zéro !');location.href='index.php';</script>";
    }
?>
<?php 
    $idd = $_GET['i'];
    $nombat = $_GET['nom'];
?>
<?php
include 'fonction.inc.php'
?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Page de détail</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
        <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <?php if($_SESSION['Role']=='Capitaine'){?>
                    <a class="btn btn-dark" href="PageBateau.php">Ajouter un bateau</a>
                    <a class="btn btn-dark" href="detail.php">Ajouter info détaillée</a>   
                <?php } ?>
                <?php if($_SESSION['Role']=='Admin'){?>
                    <a class="btn btn-dark" href="Administrer.php">Administration</a>   
                <?php } ?>
                <a class="btn btn-dark" href="deco.php">Déconnection</a>
            </div>
        </div>

        <div class="row" style="background-color: rgba(50, 50, 50, 0.5);">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                        <a href="PageAcceuil1.php"><img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/></a>
                    </div>
                </div>
            
                <br /><br /><br /><br /><br /><br />

                <div class="row" style="background-color: rgba(50, 50, 50, 0.5);">
                    <div class="col-lg-12">
                        <?php
                        $bateaux = AfficheUnBateau($idd, $nombat);
                        if ($bateaux == null) {
                            echo "<h2>Aucun bateau</h2>";
                            exit();
                        }
                        else {
                        }
                        
                        while ($bateau = mysqli_fetch_array($bateaux)) {
                            echo '<div class="row" style="background-color: black;" >';
                                echo '<div class="col-lg-5 col-sm-3" >';
                                    echo '<img class="img-fluid" style="max-height: 500px;" src="image/'.$bateau['Image'].'"/>';
                                echo '</div>';
                                echo '<div class="col-lg-7 col-sm-9" >';
                                    $details = AfficheUnDetail($idd, $nombat);
                                    if ($details == null) {
                                    }
                                    else {
                                        while ($detail = mysqli_fetch_array($details)) {
                                            $des = $detail['Description'];
                                            $pdf = $detail['pdf'];
                                        }
                                        echo '<div class="row" style="background-color: black;" >';
                                            echo '<div class="col-lg-12" style="border-style: dashed; border-color: white; text-align : center;" >';
                                                echo '<a style="color:white; font-size: 30px;" href="telecharger.php?filename=pdf/'.$pdf.'">PDF</a>';
                                            echo '</div>';
                                        echo '</div>';
                                    }
                                    echo '<div class="row" style="background-color: black;" >';
                                        echo '<div class="col-lg-12" >';
                                            echo '<h4 style="color:white;">Nom : '.$bateau['Nom'].'</h4><br />';
                                            echo '<p style="color:white;">'.$bateau['Description'].'</p><br /><br />';
                                            if ($details == null) {
                                            }
                                            else {
                                                echo '<p style="color:white;">'.$des.'</p>';
                                            }
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                            echo '<br /><br />';    
                         }
                        ?>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
<?php session_start () ?>
<?php
include 'fonction.inc.php'
?>
<?php 
    if($_SESSION['Role'] != 'Capitaine')
    {
        echo "<script>alert('Retour à zéro !');location.href='index.php';</script>";
    }
?>
<?php 
   $bateaux = getDetail($_SESSION['ID'], $_GET['nom']);
   while ($bateau = mysqli_fetch_array($bateaux))
   {
       $nomb=$bateau['Nom'];
       $descb=$bateau['Description'];
       $nomi=$bateau['pdf'];
       $idbat=$bateau['IDbateau'];
   }
?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Modifier des détails</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
    <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <a class="btn btn-dark" href="deco.php">Déconnection</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                    <a href="PageAcceuil1.php"><img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/></a>
                    </div>
                </div>
            
                <br />

                <div class="row">
                    <div class="col-lg-3 " ></div>

                    <div class="col-lg-6 text-center" style="text-align : center; background-color: rgba(238, 235, 235, 0.5); border-radius:7px;">
                        <form method="post" action="moddetail.php" enctype="multipart/form-data">
                            <br />
                            <input type="hidden" name="nom" size="30" maxlength="30" value="<?php echo $_GET['nom']; ?>"/>
                            <input type="hidden" name="tampon" size="60" maxlength="60" value="<?php echo $nomi; ?>"/>

                            <h3>Description détaillée du bateau :</h3>
                            <TEXTAREA name="desc" rows="13" cols="60" maxlength="2000"><?php echo $descb; ?></TEXTAREA>

                            <br /><br />
                            <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                            <h3>Modifier un PDF (2Mo max)</h3>
                            <input type="file" name="imag">

                            <br /><br />
                            <input type="submit" class="btn btn-dark" value="Enregistrer"></button><br />
                        </form>
                    </div>
                    <div class="col-lg-3"></div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
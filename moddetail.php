<?php session_start () ?>
<?php include 'fonction.inc.php' ?>
<?php
    $nom=$_POST['nom'];
    $descr=$_POST['desc'];
    $ima=$_FILES['imag']['name'];
    $extensions_ok = array('pdf', 'PDF');
    $extensions_fichier = substr(strrchr($_FILES['imag']['name'], '.'), 1);
    $destination = 'pdf/'.$ima;
    $imatampon =$_POST['tampon'];
    $destination2 = 'pdf/'.$imatampon;

    if(empty($nom) OR empty($descr) OR empty($ima)) 
    { 
        echo '<font color="red">Attention, un ou plusieurs champs ne sont pas remplis</font>'; 
    } 
    else
    {
            if($_FILES['imag']['size'] == 0)
            {
                echo '<font color="red">Le fichier est trop gros</font>';
            }
            else
            {
                if( !in_array($extensions_fichier, $extensions_ok))
                {
                    echo '<font color="red">Le fichier doit terminer par .pdf</font>';
                }
                else
                {
                    $testnom = getNomPdf2($ima, $imatampon);
                    if($testnom == 1)
                    {
                        echo '<font color="red">Le nom de se ficher existe déjà, veuillez changer le nom et réessayer. </font>';
                    }
                    else
                    {
                        unlink($destination2);
                        $resultat = move_uploaded_file($_FILES['imag']['tmp_name'], $destination);
                        if($resultat)
                        {
                            $con = Connection();
                            $sql = mysqli_prepare($con, 'UPDATE detail SET `Description` = "'.$descr.'", `pdf` = "'.$ima.'" WHERE Nom LIKE "'.$nom.'" AND IDbateau ='.$_SESSION['ID'].'');
                            mysqli_stmt_execute($sql);
                            mysqli_close($con);
                            echo '<font color="green">Détail bateau ajouté.</font>';
                            header('Location: PageAcceuil1.php');
                            exit();
                        }
                        else
                        {
                            echo '<font color="red">Problème lors de l\'upload, veuillez réessayer</font>'; 
                        }
                    }
                }
            }
        
    }
?>
<?php
session_start();
session_destroy();
?>
<?php
include 'fonction.inc.php'
?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Page d'accueil</title>
    <link rel="stylesheet" href="pageaccueil.css">
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <div class="container" style="max-width : 100%">
        <div class="row" style="background-color: rgba(10, 10, 10, 0.75);">
            <div class="col-lg-12 text-right">
                <a class="btn btn-dark" href="s'inscrire.php">S'inscrire</a>
                <a class="btn btn-dark" href="se connecter.php">Se connecter</a>
            </div>
        </div>
    
        <div class="row" style="background-color: rgba(50, 50, 50, 0.5);">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12" style="text-align : center;">
                        <img class="img-fluid" style="max-height: 500px;" src="image/fly.png"/>
                    </div>
                </div>
            
                <br /><br /><br /><br /><br /><br />

                <div class="row" style="background-color: rgba(50, 50, 50, 0.5);">
                    <div class="col-lg-12">
                        <?php
                        $bateaux = AfficheBateau();
                        if ($bateaux == null) {
                            echo "<h2>Aucun bateau</h2>";
                            exit();
                        }
                        else {
                        }
                        
                        while ($bateau = mysqli_fetch_array($bateaux)) {
                            echo '<div class="row" style="background-color: black;" >';
                                echo '<div class="col-lg-3 col-sm-3" >';
                                    echo '<img class="img-fluid" style="max-height: 350px;" src="image/'.$bateau['Image'].'"/>';
                                echo '</div>';
                                echo '<div class="col-lg-9 col-sm-9" >';
                                    echo '<h4 id="tex">Nom : '.$bateau['Nom'].'</h4><br />';
                                    echo '<p>'.$bateau['Description'].'</p></a><br /><br />';
                                echo '</div>';
                            echo '</div>';
                            echo '<br /><br />';    
                        }
                        ?>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    <script src="bootstrap.min.js"></script>
</body>
</html>
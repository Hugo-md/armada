<?php

function Connection()
{
    $con = mysqli_connect('localhost', 'grp_7_14', 'ceeChaht0c', 'bdd_7_14');
    if (!$con) {
        echo "Connect Error: " . mysqli_connect_error();
        exit();
    }
    return $con;
}

function AfficheBateau()
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function getNomImage($nomimage)
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        $retour = 0;
    }
    else
    {
        $retour = 0;

        if($nomimage=='Fondd.jpg' OR $nomimage=='1.jpg' OR $nomimage=='fly.png')
        {
            $retour = 1;
        }
        while ($nimage = mysqli_fetch_array($query)) {
            if($nomimage==$nimage['Image'])
            {
                $retour = 1;
            }
        }
    }
    return $retour;
}

function getNomBateau($nombateau)
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        $retour = 0;
    }
    else
    {
        $retour = 0;

        while ($nimage = mysqli_fetch_array($query)) {
            if($nombateau==$nimage['Nom'])
            {
                $retour = 1;
            }
        }
    }
    return $retour;
}

function AfficheBateauPersonne($idd)
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau WHERE IDbateau='.$idd.''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function getBateau($idd, $nomm)
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau WHERE IDbateau='.$idd.' AND Nom LIKE "'.$nomm.'"'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function getNomBateauPersonne($nombat, $nombateau, $idbat)
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau WHERE Nom NOT LIKE "'.$nombateau.'" OR IDbateau !='.$idbat.''; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        $retour = 0;
    }
    else
    {
        $retour = 0;

        while ($nimage = mysqli_fetch_array($query)) {
            if($nombat==$nimage['Nom'])
            {
                $retour = 1;
            }
        }
    }
    return $retour;
}

function AfficheUnBateau($idd, $nombat)
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau WHERE IDbateau='.$idd.' AND Nom Like "'.$nombat.'"'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function ModifierBateau($nom, $descr, $tamponnom, $tamponid)
{
    $con = Connection();
    $sql = mysqli_prepare($con, 'UPDATE bateau SET Nom ="'.$nom.'", Description = "'.$descr.'"  WHERE Nom LIKE "'.$tamponnom.'" AND IDbateau ='.$tamponid.'');
    mysqli_stmt_execute($sql);
    mysqli_close($con);
}

function Ashage($mdp)
{
    $ash = md5($mdp);
    return $ash;
}

function getPersonne($email) 
{
    $con = Connection();
    $sql = 'SELECT * FROM personne WHERE `Mail` LIKE "'.$email.'"'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;
}

function AjouterPersonne($nom, $prenom, $email, $date, $mdp)
{
    $con = Connection();
    $sql = mysqli_prepare($con, 'INSERT INTO personne(`Nom`, `Prenom`, `Mail`, `Naissance`, `Password`) VALUES ( ?, ?, ?, ?, ?)');
    mysqli_stmt_bind_param($sql, 'sssss', $nom, $prenom, $email, $date, $mdp);
    mysqli_stmt_execute($sql);
    mysqli_close($con);
}

function AffichePersonne()
{
    $con = Connection();
    $sql = 'SELECT * FROM personne'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function AfficheUnePersonne($id)
{
    $con = Connection();
    $sql = 'SELECT * FROM personne WHERE ID = "'.$id.'"'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function ModifierPersonne($rp, $id)
{
    $con = Connection();
    $sql = mysqli_prepare($con, 'UPDATE personne SET `Role` ="'.$rp.'" WHERE ID = "'.$id.'"');
    mysqli_stmt_execute($sql);
    mysqli_close($con);
}

function getNomPdf($nompdf)
{
    $con = Connection();
    $sql = 'SELECT * FROM detail'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        $retour = 0;
    }
    else
    {
        $retour = 0;

        while ($npdf = mysqli_fetch_array($query)) {
            if($nompdf==$npdf['pdf'])
            {
                $retour = 1;
            }
        }
    }
    return $retour;
}

function AfficheBateauPersonneDetail($idd)
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau WHERE IDbateau='.$idd.' AND Nom NOT IN (SELECT Nom FROM detail WHERE IDbateau='.$idd.')'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function AfficheBateauPersonneDetail2($idd)
{
    $con = Connection();
    $sql = 'SELECT * FROM bateau WHERE IDbateau='.$idd.' AND Nom IN (SELECT Nom FROM detail WHERE IDbateau='.$idd.')'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function getDetail($idd, $nom)
{
    $con = Connection();
    $sql = 'SELECT * FROM detail WHERE IDbateau='.$idd.' AND Nom LIKE "'.$nom.'"'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;
}

function getNomPdf2($nompdf, $nompdftampon)
{
    $con = Connection();
    $sql = 'SELECT * FROM detail WHERE pdf NOT LIKE "'.$nompdftampon.'"'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        $retour = 0;
    }
    else
    {
        $retour = 0;

        while ($npdf = mysqli_fetch_array($query)) {
            if($nompdf==$npdf['pdf'])
            {
                $retour = 1;
            }
        }
    }
    return $retour;
}

function AfficheUnDetail($idd, $nombat)
{
    $con = Connection();
    $sql = 'SELECT * FROM detail WHERE IDbateau='.$idd.' AND Nom Like "'.$nombat.'"'; 
    $query  = mysqli_query($con, $sql); 
    $rowCount = mysqli_num_rows($query);
    mysqli_close($con);
    
    if ($rowCount == 0) {
        return null;
    }
    return $query;

}

function DeleteDetail($nom, $id)
{
    $con = Connection();
    $sql = mysqli_prepare($con, 'DELETE FROM `detail` WHERE `Nom`LIKE "'.$nom.'" AND IDbateau ='.$id.'');
    mysqli_stmt_execute($sql);
    mysqli_close($con);
}

function UpdateNomDetail($nom, $tamponnom, $tamponid)
{
    $con = Connection();
    $sql = mysqli_prepare($con, 'UPDATE detail SET Nom ="'.$nom.'"  WHERE Nom LIKE "'.$tamponnom.'" AND IDbateau ='.$tamponid.'');
    mysqli_stmt_execute($sql);
    mysqli_close($con);
}
?>